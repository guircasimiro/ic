import os, sys, glob
import csv
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from matplotlib import cm
from colorspacious import cspace_converter
from collections import OrderedDict

# exec(open("test.py").read())


# def prepare:
#   path_e = '/home/casimiro/Guilherme/workspace/cbir3d/overall_graphics/Euclidean/'
#   path_m = '/home/casimiro/Guilherme/workspace/cbir3d/overall_graphics/Manhattan/'
#   os.mkdir(path_e)
#   os.mkdir(path_m)
#   for x in range(10, 210, 10):
#     os.mkdir(path_e + str(x) + '/')
#     os.mkdir(path_m + str(x) + '/')

# plot individual pxr

# def ind_plot(precision, recall):
#   line = plt.plot(recall, precision)
#   plt.setp(line, marker = '+', mec = 'black')
#   plt.ylim(0,1)
#   plt.xlim(0.05,1)
#   plt.gride(True)
#   # save file

line_style = ['b-s', 'g-s', 'r-s', 'c-s', 'm-s', 'y-s', 'k-s', 'b-o', 'g-o', 'r-o', 'c-o', 'm-o', 'y-o', 'k-o',
              'b-*', 'g-*', 'r-*', 'c-*', 'm-*', 'y-*', 'k-*']

classes_label = np.array(['Human', 'Cup', 'Glasses', 'Airplane', 'Ant', 'Chair', 'Octopus', 'Table',
            'Teddy', 'Hand', 'Plier', 'Fish', 'Bird', 'Armadillo', 'Bust', 'Mech',
            'Bearing', 'Vase', 'Fourleg'])
coefs = np.arange(10, 210, 10)

def plot_avg_auc(avg_p_r_m, avg_p_r_e):

  marker_e = ['s', 'o', 's', '*', 'o', 's', 'o', '*', 's', 's', 'o', 'o', '*', 'o',
                  '*', 's', 's', 'o', '*']
  line_e = ['-', '-', '-', '-', '-', '--', '--', '--', '-', ':', '-', '-', '-', ':', ':', '-.', '--', '-.', '-.']

  marker_m = ['b-s', 'g-s', 'r-s', 'c-s', 'm-s', 'y-s', 'k-s', 'b-o', 'g-o', 'r-o', 'c-o', 'm-o', 'y-o', 'k-o',
                  'b-*', 'g-*', 'r-*', 'c-*', 'm-*', 'y-*', 'k-*']
  line_m = []

  rgb_m = ['#bf4040', '#c63939', '#06f906', '#f2f20d', '#ecec13', '#f20d0d', '#e61919', '#ec1313',
           '#f9f906', '#cc3333', '#00ff00', '#06f906', '#ffff00', '#ff0000', '#f90606', '#d22d2d',
           '#0df20d', '#df2020', '#d92626']

  rgb_e = ['#b94646', '#f90606', '#00ff00', '#cc3333', '#06f906', '#d92626', '#df2020', '#f90606',
           '#ffff00', '#c63939', '#f9f906', '#0df20d', '#f2f20d', '#ec1313', '#f20d0d', '#e61919',
           '#ecec13', '#d22d2d', '#bf4040']

  plt.figure(1)
  for index in range(0, 19, 1):
    plt.plot(coefs, avg_p_r_e[:, index], rgb_e[index], label=classes_label[index])

  plt.ylabel('Average AUC')
  plt.xlim(10, 200)
  plt.xticks(np.arange(10, 210, 10), np.arange(10, 210, 10).astype(str))
  plt.title('Euclidean Distance')

  plt.legend()

  plt.figure(2)
  for index in range(0, 19, 1):
    plt.plot(coefs, avg_p_r_m[:, index], rgb_m[index], label=classes_label[index])

  plt.ylabel('Average AUC')
  plt.xlim(10, 200)
  plt.xticks(np.arange(10, 210, 10), np.arange(10, 210, 10).astype(str))
  plt.title('Manhattan Distance')
  plt.legend(classes_label, loc=1)

  plt.show()

def plot_each_coef_each_class(g_precision_e, g_recall_e, g_precision_m, g_recall_m):
  for group in range(0, 19, 1):
    plt.figure(group, figsize=(16, 9.8))
    for coef in range(0, 20, 1):
      plt.plot(g_recall_e[coef, group], g_precision_e[coef, group], line_style[coef], label=coefs[coef])
    plt.xlabel('Recall')
    plt.ylabel('Precision for each coefficient')
    plt.title('Euclidean Distance - Precision x Recall for ' + classes_label[group])
    plt.xticks(np.arange(0.05, 1.05, 0.05))
    plt.xlim(0.05, 1.0)
    plt.ylim(0.0, 1.0)
    plt.legend()
    plt.savefig('Results/PxR Group x Coef/Euclidean/' + classes_label[group] + '.png')
    plt.close()

  for group in range(0, 19, 1):
    plt.figure(group, figsize=(16, 9.8))
    for coef in range(0, 20, 1):
      plt.plot(g_recall_m[coef, group], g_precision_m[coef, group], line_style[coef], label=coefs[coef])
    plt.xlabel('Recall')
    plt.ylabel('Precision for each coefficient')
    plt.title('Manhattan Distance - Precision x Recall for ' + classes_label[group])
    plt.xticks(np.arange(0.05, 1.05, 0.05))
    plt.xlim(0.05, 1.0)
    plt.ylim(0.0, 1.0)
    plt.legend()
    plt.savefig('Results/PxR Group x Coef/Manhattan/' + classes_label[group] + '.png')
    plt.close()

def write_table(coef_group_auc_e, coef_group_auc_m):
  # with open(title + '_min' + '.csv', 'w') as csvfile:
  #   writer = csv.writer(csvfile)
  #   header = list(coefs)
  #   header.insert(0, '')
  #   writer.writerow(header)
  #   for index in range(0, 19, 1):
  #     row = list(min_auc[:, index])
  #     row.insert(0, classes_label[index])
  #     writer.writerow(row)

  with open('Tables/auc_avg_std_group' + '.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    header = (['Average', 'Standard Deviation'] * 2)
    header.insert(0, '')
    writer.writerow(header)
    for group in range(0, 19, 1):
      row = list()
      row.append(round(np.mean(coef_group_auc_e[:, group]), 3))
      row.append(round(np.std(coef_group_auc_e[: group]), 3))
      row.append(round(np.mean(coef_group_auc_m[:, group]), 3))
      row.append(round(np.std(coef_group_auc_m[:, group]), 3))
      row.insert(0, classes_label[group])
      writer.writerow(row)

  with open('Tables/auc_avg_std_coef' + '.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    header = (['Average', 'Standard Deviation'] * 2)
    header.insert(0, '')
    writer.writerow(header)
    for coef in range(0, 20, 1):
      row = list()
      row.append(round(np.mean(coef_group_auc_e[coef, :]), 3))
      row.append(round(np.std(coef_group_auc_e[coef, :]), 3))
      row.append(round(np.mean(coef_group_auc_m[coef, :]), 3))
      row.append(round(np.std(coef_group_auc_m[coef, :]), 3))
      row.insert(0, coefs[coef])
      writer.writerow(row)
  # with open(title + '_max' + '.csv', 'w') as csvfile:
  #   writer = csv.writer(csvfile)
  #   header = list(coefs)
  #   header.insert(0, '')
  #   writer.writerow(header)
  #   for index in range(0, 19, 1):
  #     row = list(max_auc[:, index])
  #     row.insert(0, classes_label[index])
  #     writer.writerow(row)



def generate_p_r(file):
  total_precision = []
  total_recall = []
  f = open(file, 'r')
  lines = f.readlines()
  f.close()
  for line in lines:
    line = line.replace('\n', '')
    array = line.split()
    precision = []
    recall = []
    find = 0
    correct = 0
    for x in array:
      find += 1
      if x == '1':
        correct += 1
        precision.append(correct/find)
        recall.append(correct/20)
    # format: 380 files x 20 (precision || recall)
    total_precision.append(precision)
    total_recall.append(recall)

  total_precision = np.array(total_precision)
  total_recall = np.array(total_recall)

  # it groups by: 19 class x 20 file x 20 (precision || recall)
  group_precision = np.array_split(total_precision, 19)
  group_recall = np.array_split(total_recall, 19)
  all_precisions.append(group_precision)
  all_recalls.append(group_recall)

  # loop through 19 groups
  for group in range(0,19):
    mean_group_p = []
    mean_group_r = []
    tmp_auc = []
    # loop through 20 hits/files
    for x in range(0,20):
      mean_group_p.append(np.mean(group_precision[group][:, x]))
      mean_group_r.append(np.mean(group_recall[group][:, x]))
      tmp_auc.append(metrics.auc(group_recall[group][x, :], group_precision[group][x, :]))

    avg_precision.append(mean_group_p)
    avg_recall.append(mean_group_r)
    avg_p_r.append(metrics.auc(mean_group_r, mean_group_p))
    auc_group.append(tmp_auc)

# All precisions and Recalls stored in here
all_precisions = []
all_recalls = []

# It stores all avg precisions, avg recalls and avg AUC
avg_precision = []
avg_recall = []
avg_p_r = []

auc_group = []
files_e = ['Euclidean/Euclidean Distance10.txt', 'Euclidean/Euclidean Distance20.txt', 'Euclidean/Euclidean Distance30.txt',
           'Euclidean/Euclidean Distance40.txt', 'Euclidean/Euclidean Distance50.txt', 'Euclidean/Euclidean Distance60.txt',
           'Euclidean/Euclidean Distance70.txt', 'Euclidean/Euclidean Distance80.txt', 'Euclidean/Euclidean Distance90.txt',
           'Euclidean/Euclidean Distance100.txt', 'Euclidean/Euclidean Distance110.txt', 'Euclidean/Euclidean Distance120.txt',
           'Euclidean/Euclidean Distance130.txt', 'Euclidean/Euclidean Distance140.txt', 'Euclidean/Euclidean Distance150.txt',
           'Euclidean/Euclidean Distance160.txt', 'Euclidean/Euclidean Distance170.txt', 'Euclidean/Euclidean Distance180.txt',
           'Euclidean/Euclidean Distance190.txt', 'Euclidean/Euclidean Distance200.txt']

files_m = ['Manhattan/Manhattan Distance10.txt', 'Manhattan/Manhattan Distance20.txt', 'Manhattan/Manhattan Distance30.txt',
           'Manhattan/Manhattan Distance40.txt', 'Manhattan/Manhattan Distance50.txt', 'Manhattan/Manhattan Distance60.txt',
           'Manhattan/Manhattan Distance70.txt', 'Manhattan/Manhattan Distance80.txt', 'Manhattan/Manhattan Distance90.txt',
           'Manhattan/Manhattan Distance100.txt', 'Manhattan/Manhattan Distance110.txt', 'Manhattan/Manhattan Distance120.txt',
           'Manhattan/Manhattan Distance130.txt', 'Manhattan/Manhattan Distance140.txt', 'Manhattan/Manhattan Distance150.txt',
           'Manhattan/Manhattan Distance160.txt', 'Manhattan/Manhattan Distance170.txt', 'Manhattan/Manhattan Distance180.txt',
           'Manhattan/Manhattan Distance190.txt', 'Manhattan/Manhattan Distance200.txt']


# loop through 20 coeficients in Euclidean Distance
for file in files_e:
  generate_p_r(file)

# loop through 20 coeficients in Manhattan Distance
for file in files_m:
  generate_p_r(file)

all_precisions_e = all_precisions[0:20]
all_recalls_e = all_recalls[0:20]
all_precisions_m = all_precisions[20:40]
all_recalls_m = all_recalls[20:40]

auc_group_e = auc_group[0:380]
auc_group_m = auc_group[380:760]



auc_coef_e = np.array_split(auc_group_e, 20)
auc_coef_m = np.array_split(auc_group_m, 20)
max_auc_e = []
max_auc_m = []
min_auc_e = []
min_auc_m = []

for coef in range(0,20):
  for group in range(0,19):
    max_auc_e.append(max(auc_coef_e[coef][group]))
    max_auc_m.append(max(auc_coef_m[coef][group]))
    min_auc_e.append(min(auc_coef_e[coef][group]))
    min_auc_m.append(min(auc_coef_m[coef][group]))

max_auc_e = np.array(np.array_split(max_auc_e, 20))
max_auc_m = np.array(np.array_split(max_auc_m, 20))
min_auc_e = np.array(np.array_split(min_auc_e, 20))
min_auc_m = np.array(np.array_split(min_auc_m, 20))


avg_precision_e = avg_precision[0:380]
avg_precision_m = avg_precision[380:760]

avg_recall_e = avg_recall[0:380]
avg_recall_m = avg_recall[380:760]

group_precision_e = np.array(np.array_split(avg_precision_e, 19))
group_precision_m = np.array(np.array_split(avg_precision_m, 19))
group_recall_e = np.array(np.array_split(avg_recall_e, 19))
group_recall_m = np.array(np.array_split(avg_recall_m, 19))

coef_precision_e = np.array(np.array_split(avg_precision_e, 20))
coef_precision_m = np.array(np.array_split(avg_precision_m, 20))
coef_recall_e = np.array(np.array_split(avg_recall_e, 20))
coef_recall_m = np.array(np.array_split(avg_recall_m, 20))

avg_p_r_e = avg_p_r[0:380]
avg_p_r_m = avg_p_r[380:760]

coefs = np.arange(10, 210, 10)
classes_label = np.array(['Human', 'Cup', 'Glasses', 'Airplane', 'Ant', 'Chair', 'Octopus', 'Table',
            'Teddy', 'Hand', 'Plier', 'Fish', 'Bird', 'Armadillo', 'Bust', 'Mech',
            'Bearing', 'Vase', 'Fourleg'])

avg_coef_p_r_e = np.array(np.array_split(avg_p_r_e, 20))
avg_coef_p_r_m = np.array(np.array_split(avg_p_r_m, 20))
avg_group_p_r_e = np.array(np.array_split(avg_p_r_e, 19))
avg_group_p_r_m = np.array(np.array_split(avg_p_r_m, 19))


'''
Generate class plots: 
plot_each_coef_each_class(coef_precision_e, coef_recall_e, coef_precision_m, coef_recall_m)

Generate table:
write_table(avg_coef_p_r_e, avg_coef_p_r_m)

Generate avg plot:
plot_avg_auc(avg_coef_p_r_m, avg_coef_p_r_e)

Generate best_worst plot

best scenario: avg_coef_p_r_m[12][2] - Glass 130
  best_scenario_p = all_precisions_m[12][2]
  best_scenario_r = all_recalls_m[12][2]
  best_tmp = []
  for x in range(0,20):
    best_tmp.append(metrics.auc(best_scenario_r[x,:], best_scenario_p[x,:]))
  Best Input: np.argmax(best_tmp) # 0 -> 61.off
  Worst Input: np.argmin(best_tmp) # 9 -> 70.off

worst scenario: avg_coef_p_r_m[15][13] - Armadillo 160
  worst_scenario_p = all_precisions_m[15][13]
  worst_scenario_r = all_recalls_m[15][13]
  worst_tmp = []
  for x in range(0,20):
    worst_tmp.append(metrics.auc(worst_scenario_r[x,:], worst_scenario_p[x,:]))
  Best Input: np.argmax(worst_tmp) # 0 -> 261.off
  Worst Input: np.argmin(worst_tmp) # 8 -> 269.off


plt.figure()
plt.title('Precision versus Recall curve')
plt.ylabel('Precision')
plt.xlim(0.0, 1.0)
plt.xlabel('Recall')
plt.plot(best_scenario_r[0], best_scenario_p[0], label='Object 61')
plt.plot(best_scenario_r[9], best_scenario_p[9], label='Object 70')
plt.plot(worst_scenario_r[0], worst_scenario_p[0], label='Object 261')
plt.plot(worst_scenario_r[8], worst_scenario_p[8], label='Object 269')
plt.legend()
plt.show()

'''


'''
line_style = ['xkcd:blue', 'xkcd:purple', 'xkcd:pink', 'xkcd:brown', 'xkcd:red', 'xkcd:purple',
              'xkcd:light blue', 'xkcd:teal', 'xkcd:orange', 'xkcd:light green', 'xkcd:magenta', 'xkcd:yellow',
              'xkcd:grey', 'xkcd:light purple', 'xkcd:turquoise', 'xkcd:tan', 'xkcd:cyan', 'xkcd:hot pink',
              'xkcd:peach']

line_style = ['b-s', 'g-s', 'r-s', 'c-s', 'm-s', 'y-s', 'k-s', 'b-o', 'g-o', 'r-o', 'c-o', 'm-o', 'y-o', 'k-o',
              'b-*', 'g-*', 'r-*', 'c-*', 'm-*', 'y-*', 'k-*']

for index in range(0, 19, 1):
  plt.plot(coefs, avg_p_r_e[:, index], line_style[index], label=classes_label[index])

plt.ylabel('Average AUC')
plt.xlim(10, 200)
plt.xticks(np.arange(10, 210, 10), np.arange(10, 210, 10).astype(str))
plt.title('Euclidean Distance')

plt.legend()
plt.show()

# groups every array into format: 20 coef x 19 class x 20 hit
avg_precision_e = np.array_split(avg_precision_e, 20)
avg_precision_m = np.array_split(avg_precision_m, 20)
avg_recall_e = np.array_split(avg_recall_e, 20)
avg_recall_m = np.array_split(avg_recall_m, 20)


classes = np.array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19])

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')



# for index in range(0, 20, 1):
#   ax.plot(np.repeat(coefs[index], 19), classes, avg_p_r_m[index], zdir='z', label=str(coefs[index]))

# ax.set_zlabel('AUC')
# ax.set_ylim(1, 19)
# ax.set_xlim(10, 200)
# ax.set_zlim(0, 1)
# plt.yticks(classes, classes_label)
# plt.xticks(coefs, coefs, rotation=45)
# plt.legend()
# plt.show()

f, axes = plt.subplots(2, 2, sharey='all', sharex='all')

for index in range(0, 10, 1):
  axes[0,0].plot(coefs, avg_p_r_e[:, index], label=classes_label[index])

for index in range(10, 19, 1):
  axes[0,1].plot(coefs, avg_p_r_e[:, index], label=classes_label[index])

for index in range(0, 10, 1):
  axes[1,0].plot(coefs, avg_p_r_m[:, index], label=classes_label[index])

for index in range(10, 19, 1):
  axes[1,1].plot(coefs, avg_p_r_m[:, index], label=classes_label[index])


plt.ylabel('Average AUC')
plt.xlim(10, 200)
plt.xticks(np.arange(10, 210, 10), np.arange(10, 210, 10).astype(str))

axes[0,0].set_title('Euclidean Distance (first 10 classes)')
axes[0,0].legend()

axes[0,1].set_title('Euclidean Distance (last 9 classes)')
axes[0,1].legend()

axes[1,0].set_title('Manhattan Distance (first 10 classes)')
axes[1,0].legend()

axes[1,1].set_title('Euclidean Distance (first 10 classes)')
axes[1,1].legend()
plt.show()

'''